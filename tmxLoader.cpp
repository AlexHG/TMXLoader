#include "tmxLoader.hpp"
#include "tinyxml2.h"


template<>
EnumParser<BLUE_GRASS_TILE_TYPES>::EnumParser()
{
    enumMap["NOTHING"] = NOTHING;
    enumMap["BLUE_GRASS_LEFT_CORNER_1"] = BLUE_GRASS_LEFT_CORNER_1;
    enumMap["BLUE_GRASS_1"] = BLUE_GRASS_1;
    enumMap["BLUE_GRASS_2"] = BLUE_GRASS_2;
    enumMap["BLUE_GRASS_3"] = BLUE_GRASS_3;
    enumMap["BLUE_GRASS_4"] = BLUE_GRASS_4;
    enumMap["BLUE_GRASS_5"] = BLUE_GRASS_5;
    enumMap["BLUE_GRASS_RIGHT_CORNER_1"] = BLUE_GRASS_RIGHT_CORNER_1;
    enumMap["NOTHING_2"] = NOTHING_2;
    enumMap["BLUE_GRASS_LEFT_CORNER_2"] = BLUE_GRASS_LEFT_CORNER_2;
    enumMap["BLUE_GRASS_6"] = BLUE_GRASS_6;
    enumMap["BLUE_GRASS_7"] = BLUE_GRASS_7;
    enumMap["BLUE_GRASS_8"] = BLUE_GRASS_8;
    enumMap["BLUE_GRASS_9"] = BLUE_GRASS_9;
    enumMap["BLUE_GRASS_10"] = BLUE_GRASS_10;
    enumMap["BLUE_GRASS_RIGHT_CORNER_2"] = BLUE_GRASS_RIGHT_CORNER_2;
    enumMap["NOTHING_3"] = NOTHING_3;
    enumMap["BLUE_GRASS_LEFT_CORNER_3"] = BLUE_GRASS_LEFT_CORNER_3;
    enumMap["BLUE_GRASS_11"] = BLUE_GRASS_11;
    enumMap["BLUE_GRASS_12"] = BLUE_GRASS_12;
    enumMap["BLUE_GRASS_13"] = BLUE_GRASS_13;
    enumMap["BLUE_GRASS_14"] = BLUE_GRASS_14;
    enumMap["BLUE_GRASS_15"] = BLUE_GRASS_15;
    enumMap["BLUE_GRASS_RIGHT_CORNER_3"] = BLUE_GRASS_RIGHT_CORNER_3;
    enumMap["NOTHING_4"] = NOTHING_4;
    enumMap["BLUE_GRASS_LEFT_CORNER_4"] = BLUE_GRASS_LEFT_CORNER_4;
    enumMap["BLUE_GRASS_16"] = BLUE_GRASS_16;
    enumMap["BLUE_GRASS_17"] = BLUE_GRASS_17;
    enumMap["BLUE_GRASS_18"] = BLUE_GRASS_18;
    enumMap["BLUE_GRASS_19"] = BLUE_GRASS_19;
    enumMap["BLUE_GRASS_20"] = BLUE_GRASS_20;
    enumMap["BLUE_GRASS_RIGHT_CORNER_4"] = BLUE_GRASS_RIGHT_CORNER_4;
	enumMap["NOTHING_5"] = NOTHING_5;
    enumMap["BLUE_GRASS_LEFT_CORNER_5"] = BLUE_GRASS_LEFT_CORNER_5;
    enumMap["BLUE_GRASS_21"] = BLUE_GRASS_21;
    enumMap["BLUE_GRASS_22"] = BLUE_GRASS_22;
    enumMap["BLUE_GRASS_23"] = BLUE_GRASS_23;
    enumMap["BLUE_GRASS_24"] = BLUE_GRASS_24;
    enumMap["BLUE_GRASS_25"] = BLUE_GRASS_25;
    enumMap["BLUE_GRASS_RIGHT_CORNER_5"] = BLUE_GRASS_RIGHT_CORNER_5;
	enumMap["NOTHING_6"] = NOTHING_6;
    enumMap["BLUE_GRASS_LEFT_CORNER_6"] = BLUE_GRASS_LEFT_CORNER_6;
    enumMap["BLUE_GRASS_26"] = BLUE_GRASS_26;
    enumMap["BLUE_GRASS_27"] = BLUE_GRASS_27;
    enumMap["BLUE_GRASS_28"] = BLUE_GRASS_28;
    enumMap["BLUE_GRASS_29"] = BLUE_GRASS_29;
    enumMap["BLUE_GRASS_30"] = BLUE_GRASS_30;
    enumMap["BLUE_GRASS_RIGHT_CORNER_6"] = BLUE_GRASS_RIGHT_CORNER_6;
}



TMXLoader::~TMXLoader()
{

}

void TMXLoader::loadFile(std::string file)
{
	//load the file using the the directory functions 
	using namespace tinyxml2;
	std::string filePath = Directory::testFile(file);
	
	//make a tinyXML2 instance
	XMLDocument doc;
	doc.LoadFile(filePath.c_str());

	//get amount of tiles
	XMLElement *rootEle = doc.FirstChildElement("map");
	if(rootEle != NULL)
	{
		rootEle->QueryIntAttribute("width", &pHorizontalTiles);
		rootEle->QueryIntAttribute("height", &pVerticalTiles);	
		std::cout << "width: "<< pHorizontalTiles << " Heigth: "<< pVerticalTiles << std::endl;			   
	}

	//get the  background name
	XMLElement *backgroundData = doc.FirstChildElement("map")->FirstChildElement("imagelayer")->FirstChildElement();
	if(backgroundData != NULL)
	{
		pBackground = backgroundData->Attribute("source");
		std::cout << "bg name: " << pBackground << std::endl;
	}

	//instanciate the enumParser class with the enum you want to resolve
	EnumParser<BLUE_GRASS_TILE_TYPES> parser;
	
	//locate the tile information on the tmx file
	XMLElement *tileTypes = doc.FirstChildElement("map")->FirstChildElement("tileset")->FirstChildElement()->NextSiblingElement();
	for(XMLElement *e = tileTypes; e != NULL; e = e->NextSiblingElement())
	{	
		//get the ID that the tmx file gave that TILE (based on it sposition on the tile sheet
		int ID;
		e->QueryIntAttribute("id", &ID);
		//now dig into the self created attributes
		XMLElement *t = e->FirstChildElement()->FirstChildElement();
		if(t != NULL)
		{
			//get the attribute name and value
			std::string name = t->Attribute("name"); 
			std::string valueString = t->Attribute("value");			
		
			//populate the map using the tile ID that the map editor asigned the tile, and the ID string you assigned
			IDtoString[ID] = valueString;
		}

	}
	
	//this will print the map and its values, useful for debugging
	for(auto iterator1 = IDtoString.begin(); iterator1 != IDtoString.end(); ++iterator1)
	{
		std::cout << "ID: " << iterator1->first << " String: " << iterator1->second << std::endl;
	}



	//get the tile values and populate our tile list
	XMLElement *tileData = doc.FirstChildElement("map")->FirstChildElement("layer")->FirstChildElement("data")->FirstChildElement("tile");
	if(tileData != NULL)
	{
		for(XMLElement *e = tileData; e != NULL; e = e->NextSiblingElement("tile"))
		{			
			int gid = 0;
			e->QueryIntAttribute("gid", &gid);
			//this 'gid' has the same value that the ID we used to populate the map with
			pTileList.push_back(gid);			
		}
	}
	
	std::cout << "amount of active tiles : " << pTileList.size() << std::endl;	
}

std::string TMXLoader::getBackgroundName()
{
	return pBackground;
}

int TMXLoader::getLevelWidth()
{
	return pHorizontalTiles;
}

int TMXLoader::getLevelHeigth()
{
	return pVerticalTiles;
}

std::list<int> TMXLoader::getTileList()
{
	return pTileList;
}


void TMXLoader::generateLevel(std::list<int> &tiles)
{
	//iterator for the tile vector
	std::list<int>::iterator tileIterator = pTileList.begin();
	std::cout << "amount of active tiles : " << pTileList.size() << std::endl;

	//string to enum converter, this will take in the string from our map and return a valid enum value from our tile enum
	EnumParser<BLUE_GRASS_TILE_TYPES> parser;

	for(int i = 0; i < pVerticalTiles; ++i)
	{
		for(int j = 0; j < pHorizontalTiles; ++j)
		{
/*NOTE:
  For some reason tiled map editor assigns the value of 0 to tiles that are 'empty' (tile that you did not place a tile over). At the same time it assigns
  IDs to your tile sheet based on position, so the first tile in your sprite sheet will have an ID of 0. This will fuck up your map, to fix this I simply 
  left the first space in the tile sheet empty when a 'gid' that isn't 0 is on the list, I simply apply an offset of -1.*/
			if(*tileIterator != 0 )
			{
				//here you can initialize each tile with the data at hand, in this example I will just insert the enum-converted tile IDs
				int offset = *tileIterator - 1;
				tiles.push_back(parser.resolveEnum(IDtoString[offset]));
				std::cout << "Tile added: " << offset << std::endl;
			}
			++tileIterator;
		}
	}


}

