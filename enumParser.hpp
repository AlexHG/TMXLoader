#include <iostream>
#include <string>
#include <map>
#include <stdexcept>
#include "ID.hpp"

using namespace std;

template <typename T> class EnumParser
{
	std::map<std::string, T> enumMap;
public:
    EnumParser(){};

    T resolveEnum(const std::string &value)
    { 
        typename map <std::string, T>::const_iterator iValue = enumMap.find(value);
		std::cout << "Looking for " << value << " in map." << std::endl;
        if (iValue  == enumMap.end())
            throw runtime_error("");
        return iValue->second;
    }
};



