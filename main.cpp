
#include <list>
#include <iostream>

#include "tmxLoader.hpp"

int main()
{
	std::shared_ptr<TMXLoader>level(new TMXLoader);
	std::string lvlpath = "map.tmx";
	level->loadFile(lvlpath);
	std::list<int> tileList;
	level->generateLevel(tileList);

	std::cout << "Amount of tiles loaded: " << tileList.size() << std::endl;
	
}
